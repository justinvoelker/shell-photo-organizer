module.exports = {
  clearMocks: true,
  preset: "ts-jest",
  resetMocks: true,
  restoreMocks: true,
  testEnvironment: "node",
};
