import { config as getConfig } from "./helpers/config";
import { logger } from "./helpers/logger";
import {
  getMetadataExtendedFilenameDate,
  getMetadataExifTool,
  getMetadataExtendedDates,
  replaceFileExtension,
  replaceMetadataReferences,
} from "./helpers/metadata";
import {
  createDirectory,
  fileExists,
  getFiles,
  renameAndMoveFile,
} from "./helpers/shell";
import {
  ensureSidecarFileExists as ensureRequiredSidecarFileExists,
  getSidecarFilename,
} from "./helpers/sidecar";
import { getListHash } from "./utils/crypto";
import { matchesPattern } from "./utils/regex";
import { stripExtraSlashes } from "./utils/string";

const config = getConfig();
loop();
setInterval(loop, config.intervalSeconds * 1000);

let fileListHashes: Record<string, string> = {};

async function loop() {
  try {
    // Loop through each task
    for (const task of config.tasks) {
      // Set logger log level
      logger.level = task.logLevel || config.logLevel;
      logger.verbose(`Task "${task.name}" log level set to "${logger.level}"`);

      // Skip everything if task is disabled
      if (!task.enabled) {
        logger.verbose(`Task "${task.name}" is disabled`);
        continue;
      }

      // Log start of task
      logger.verbose(`Task "${task.name}" started`);

      // Get directory names
      const srcDirectory = `/${stripExtraSlashes(task.srcDirectory)}/`;
      const dstDirectoryRaw = `/${stripExtraSlashes(task.dstDirectory)}/`;

      // Get source files
      const srcFiles = getFiles({
        directory: srcDirectory,
        recursive: true,
      });

      // Directory/task hash key to ensure this specific directory and task don't reprocess this list of files
      const listHashKey = `${srcDirectory}_${task.name}`;

      // Only need to process files if the directories files have changed
      if (
        fileListHashes !== undefined &&
        getListHash(srcFiles) == fileListHashes[listHashKey]
      ) {
        logger.verbose(`Task "${task.name}" is finishing (no file changes)`);
        continue;
      }

      // Loop through each source file
      for (const srcFile of srcFiles) {
        const srcFileDir = srcFile.split("/").slice(1, -1).join("/");
        const srcFileName = srcFile.split("/")[srcFile.split("/").length - 1];

        // Each file will pass/fail on its own
        try {
          // Check if source file matches source pattern
          if (!matchesPattern(srcFileName, task.srcFilenamePattern)) {
            logger.verbose(
              `File "${srcFileName}" does not match pattern ${task.srcFilenamePattern}`
            );
            continue;
          }

          // Continue processing source file
          logger.verbose(`File "${srcFileName}" started`);

          // Get metadata for source file
          const metadataExiftool = await getMetadataExifTool({ path: srcFile });
          const metadataExtendedDates = getMetadataExtendedDates({
            exifToolTags: metadataExiftool,
            metadataModifiers: task.metadataModifiers,
          });
          const metadataExtendedFilenameDate = getMetadataExtendedFilenameDate({
            exiftoolMetadata: metadataExiftool,
            filename: srcFileName as string,
            pattern: task.srcFilenameDateTimePattern,
            replace: task.srcFilenameDateTimeReplace,
            tzParse: task.srcFilenameDateTimeTimezoneParse,
            tzLocal: task.srcFilenameDateTimeTimezoneLocal,
          });
          const metadata = {
            exiftool: metadataExiftool,
            extended: {
              ...metadataExtendedDates,
              ...metadataExtendedFilenameDate,
            },
          };
          logger.debug(
            `File "${srcFile}" metadata: ${JSON.stringify(metadata)}`
          );

          // Replace metadata within destination directory
          const dstDirectory = replaceMetadataReferences({
            value: dstDirectoryRaw,
            metadata,
          });

          // Create destination directory
          createDirectory({
            path: dstDirectory,
            dryRun: task.dryRun,
          });

          // Begin destination filename renaming
          let dstFileName = srcFileName as string;

          // Change case of file extension
          dstFileName = replaceFileExtension({
            filename: dstFileName,
            extensionCase: task.dstFilenameExtensionCase,
            extensionPattern: task.dstFilenameExtensionCasePattern,
          });

          // Loop through each step
          for (const step of task.steps) {
            logger.verbose(`Step "${step.name}" old value "${dstFileName}"`);
            const stepFind = new RegExp(step.find, step.flag);
            const stepRepl = replaceMetadataReferences({
              value: step.repl,
              metadata,
            });
            dstFileName = dstFileName.replace(stepFind, stepRepl);
            logger.verbose(`Step "${step.name}" new value "${dstFileName}"`);
          }

          // Ensure required sidecar files exist
          for (const sidecar of task.sidecars) {
            ensureRequiredSidecarFileExists({
              primaryFileName: srcFileName,
              primaryFilePath: srcFileDir,
              findPrimaryPattern: sidecar.findPrimaryPattern,
              findSidecarReplace: sidecar.findSidecarReplace,
              required: sidecar.required,
            });
          }

          // Rename and move file
          renameAndMoveFile({
            dryRun: task.dryRun,
            srcFilePath: srcFile,
            dstFilePath: `${dstDirectory}/${dstFileName}`,
          });

          // Rename and move sidecar files
          for (const sidecar of task.sidecars) {
            const sidecarFilenameOld = getSidecarFilename({
              name: srcFileName,
              find: sidecar.findPrimaryPattern,
              replace: sidecar.findSidecarReplace,
            });
            const sidecarFilenameNew = getSidecarFilename({
              name: dstFileName,
              find: sidecar.namePrimaryPattern,
              replace: sidecar.nameSidecarReplace,
            });
            const sidecarFileExists = fileExists({
              path: `/${stripExtraSlashes(srcFileDir)}/${sidecarFilenameOld}`,
            });
            if (sidecarFileExists) {
              renameAndMoveFile({
                dryRun: task.dryRun,
                srcFilePath: `/${stripExtraSlashes(
                  srcFileDir
                )}/${sidecarFilenameOld}`,
                dstFilePath: `${dstDirectory}/${sidecarFilenameNew}`,
              });
            }
          }
        } catch (err) {
          logger.error(err.toString());
        }
      }

      // Hash a list of all current files for comparison at start of next loop
      fileListHashes = {
        ...fileListHashes,
        [listHashKey]: getListHash(srcFiles),
      };

      // Log completion of task
      logger.verbose(`Task "${task.name}" completed`);
    }
  } catch (err) {
    logger.error(err.toString());
  }
}
