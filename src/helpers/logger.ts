import fs from "fs";
import winston from "winston";
import { config } from "./config";

export const logger = winston.createLogger({
  level: config().logLevel,
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
      ),
    }),
  ],
});

export function logEdits(
  action: string,
  from: string,
  to: string,
  dryRun: boolean
): void {
  const year = new Date().getFullYear();
  const dryRunAppend = dryRun ? ".dryrun" : "";
  const logDirectory = "/var/log";
  const logFilename = `/edits-${year}${dryRunAppend}.log`;

  const message = {
    time: new Date().toISOString(),
    action,
    from,
    to,
  };

  fs.appendFileSync(
    `${logDirectory}${logFilename}`,
    `${JSON.stringify(message)}\n`
  );
}
