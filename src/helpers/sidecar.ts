import { stripExtraSlashes } from "../utils/string";
import { logger } from "./logger";
import { fileExists } from "./shell";

import * as sidecar from "./sidecar";

export function ensureSidecarFileExists({
  primaryFileName,
  primaryFilePath,
  findPrimaryPattern,
  findSidecarReplace,
  required,
}: {
  primaryFileName: string;
  primaryFilePath: string;
  findPrimaryPattern: string;
  findSidecarReplace: string;
  required: boolean;
}): void {
  logger.debug(
    `sidecar.ensureSidecarFilesExist({primaryFileName: "${primaryFileName}", primaryFilePath: "${primaryFilePath}", findPrimaryPattern: "${findPrimaryPattern}", findSidecarReplace: "${findSidecarReplace}, required: "${required}"})`
  );

  if (required) {
    const sidecarFilename = sidecar.getSidecarFilename({
      name: primaryFileName,
      find: findPrimaryPattern,
      replace: findSidecarReplace,
    });

    const sidecarExists = fileExists({
      path: `/${stripExtraSlashes(primaryFilePath)}/${sidecarFilename}`,
    });

    if (!sidecarExists) {
      throw `Unable to find sidecar file ${sidecarFilename} for primary file ${primaryFileName}`;
    }
  }
}

export function getSidecarFilename({
  name,
  find,
  replace,
}: {
  name: string;
  find: string;
  replace: string;
}): string {
  logger.debug(
    `sidecar.getSidecarFilename({name: "${name}", find: "${find}", replace: "${replace}"})`
  );

  return name.replace(new RegExp(find), replace);
}
