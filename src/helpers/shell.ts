import shelljs from "shelljs";
import { logger, logEdits } from "./logger";

export function createDirectory({
  dryRun,
  path,
}: {
  dryRun: boolean;
  path: string;
}): void {
  logger.debug(`shell.createDirectory({dryRun: ${dryRun}, path: "${path}"})`);
  logger.verbose(`Create directory "${path}"`, { dryRun });
  if (dryRun) return;
  shelljs.mkdir("-p", path);
}

export function fileExists({ path }: { path: string }): boolean {
  logger.debug(`shell.fileExists({path: "${path}"})`);
  return shelljs.test("-e", path);
}

export function getFiles({
  directory,
  recursive = false,
}: {
  directory: string;
  recursive?: boolean;
}): Array<string> {
  logger.debug(
    `shell.getFiles({directory: "${directory}", recursive: ${recursive}})`
  );
  logger.verbose(`Read "${directory}" directory`);
  const list = recursive ? shelljs.ls("-R", directory) : shelljs.ls(directory);
  return list
    .filter((path) => shelljs.test("-f", `${directory}${path}`))
    .map((path) => `${directory}${path}`);
}

export function renameAndMoveFile({
  dryRun,
  srcFilePath,
  dstFilePath,
}: {
  dryRun: boolean;
  srcFilePath: string;
  dstFilePath: string;
}): void {
  logger.debug(
    `shell.renameAndMoveFile({dryRun: ${dryRun}, srcFilePath: "${srcFilePath}", dstFilePath: "${dstFilePath}"})`
  );

  let renamed = false;
  let moved = false;

  const srcFileDir = `/${srcFilePath.split("/").slice(1, -1).join("/")}`;
  const srcFileName = srcFilePath.split("/")[srcFilePath.split("/").length - 1];
  const dstFileDir = `/${dstFilePath.split("/").slice(1, -1).join("/")}`;
  const dstFileName = dstFilePath.split("/")[dstFilePath.split("/").length - 1];

  if (dstFileName != srcFileName || dstFileDir != srcFileDir) {
    if (!dryRun) {
      shelljs.mv("-n", srcFilePath, dstFilePath);
    }
    renamed = dstFileName != srcFileName ? true : false;
    moved = dstFileDir != srcFileDir ? true : false;
  }

  if (renamed || moved) {
    logger.info(`RenameAndMove "${srcFilePath}" to "${dstFilePath}"`, {
      dryRun,
    });
    logEdits("RenameAndMove", srcFilePath, dstFilePath, dryRun);
  }
}
