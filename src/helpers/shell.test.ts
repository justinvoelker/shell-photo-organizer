import shelljs, { ShellArray } from "shelljs";

import { logger } from "./logger";
import {
  createDirectory,
  fileExists,
  getFiles,
  renameAndMoveFile,
} from "./shell";

jest.mock("./logger");

jest.mock("./config", () => ({
  config: jest.fn(() => {
    return {
      logLevel: "warn",
    };
  }),
}));

describe("createDirectory", () => {
  it("should only console log action during a dry run", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerVerboseSpy = jest.spyOn(logger, "verbose").mockImplementation();
    const shelljsMkdirSpy = jest.spyOn(shelljs, "mkdir").mockImplementation();

    createDirectory({
      dryRun: true,
      path: "/path",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerVerboseSpy).toHaveBeenCalledTimes(1);
    expect(shelljsMkdirSpy).toHaveBeenCalledTimes(0);
  });

  it("should execute mkdir command", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerVerboseSpy = jest.spyOn(logger, "verbose").mockImplementation();
    const shelljsMkdirSpy = jest.spyOn(shelljs, "mkdir").mockImplementation();

    createDirectory({
      dryRun: false,
      path: "/path",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerVerboseSpy).toHaveBeenCalledTimes(1);
    expect(shelljsMkdirSpy).toHaveBeenCalledTimes(1);
    expect(shelljsMkdirSpy).toHaveBeenCalledWith("-p", "/path");
  });
});

describe("fileExists", () => {
  it("should return true if the file exists", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const shelljsTestSpy = jest.spyOn(shelljs, "test").mockReturnValue(true);

    const exists = fileExists({ path: "/path/file.ext" });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(exists).toBe(true);
    expect(shelljsTestSpy).toHaveBeenCalledTimes(1);
    expect(shelljsTestSpy).toHaveBeenCalledWith("-e", "/path/file.ext");
  });

  it("should return false if the file does not exist", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const shelljsTestSpy = jest.spyOn(shelljs, "test").mockReturnValue(false);

    const exists = fileExists({ path: "/path/file.ext" });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(exists).toBe(false);
    expect(shelljsTestSpy).toHaveBeenCalledTimes(1);
    expect(shelljsTestSpy).toHaveBeenCalledWith("-e", "/path/file.ext");
  });
});

describe("getFiles", () => {
  it("should return single directory of files by default", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerVerboseSpy = jest.spyOn(logger, "verbose").mockImplementation();
    const shelljsLsSpy = jest
      .spyOn(shelljs, "ls")
      .mockReturnValue(["file0.ext"] as ShellArray);
    const shelljsTestSpy = jest
      .spyOn(shelljs, "test")
      .mockReturnValueOnce(true);

    const files = getFiles({
      directory: "/path/",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerVerboseSpy).toHaveBeenCalledTimes(1);
    expect(shelljsLsSpy).toHaveBeenCalledTimes(1);
    expect(shelljsLsSpy).toHaveBeenCalledWith("/path/");
    expect(shelljsTestSpy).toHaveBeenCalledTimes(1);
    expect(files).toStrictEqual(["/path/file0.ext"]);
  });

  it("should return single directory of files", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerVerboseSpy = jest.spyOn(logger, "verbose").mockImplementation();
    const shelljsLsSpy = jest
      .spyOn(shelljs, "ls")
      .mockReturnValue(["file0.ext"] as ShellArray);
    const shelljsTestSpy = jest
      .spyOn(shelljs, "test")
      .mockReturnValueOnce(true)
      .mockReturnValueOnce(false)
      .mockReturnValueOnce(true);

    const files = getFiles({
      directory: "/path/",
      recursive: false,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerVerboseSpy).toHaveBeenCalledTimes(1);
    expect(shelljsLsSpy).toHaveBeenCalledTimes(1);
    expect(shelljsLsSpy).toHaveBeenCalledWith("/path/");
    expect(shelljsTestSpy).toHaveBeenCalledTimes(1);
    expect(files).toStrictEqual(["/path/file0.ext"]);
  });

  it("should return multiple directories of files", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerVerboseSpy = jest.spyOn(logger, "verbose").mockImplementation();
    const shelljsLsSpy = jest
      .spyOn(shelljs, "ls")
      .mockReturnValue(["file0.ext", "dir", "dir/file1.ext"] as ShellArray);
    const shelljsTestSpy = jest
      .spyOn(shelljs, "test")
      .mockReturnValueOnce(true)
      .mockReturnValueOnce(false)
      .mockReturnValueOnce(true);

    const files = getFiles({
      directory: "/path/",
      recursive: true,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerVerboseSpy).toHaveBeenCalledTimes(1);
    expect(shelljsLsSpy).toHaveBeenCalledTimes(1);
    expect(shelljsLsSpy).toHaveBeenCalledWith("-R", "/path/");
    expect(shelljsTestSpy).toHaveBeenCalledTimes(3);
    expect(files).toStrictEqual(["/path/file0.ext", "/path/dir/file1.ext"]);
  });
});

describe("renameAndMoveFile", () => {
  it("should do nothing if directories match and file names match", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    const shelljsMvSpy = jest.spyOn(shelljs, "mv").mockImplementation();

    renameAndMoveFile({
      dryRun: false,
      srcFilePath: "/Dir/FileName.jpg",
      dstFilePath: "/Dir/FileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(0);
    expect(shelljsMvSpy).toHaveBeenCalledTimes(0);
  });

  it("should execute mv command if directories match and file names differ", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    const shelljsMvSpy = jest.spyOn(shelljs, "mv").mockImplementation();

    renameAndMoveFile({
      dryRun: false,
      srcFilePath: "/Dir/SrcFileName.jpg",
      dstFilePath: "/Dir/DstFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(shelljsMvSpy).toHaveBeenCalledTimes(1);
    expect(shelljsMvSpy).toHaveBeenCalledWith(
      "-n",
      "/Dir/SrcFileName.jpg",
      "/Dir/DstFileName.jpg"
    );
  });

  it("should execute mv command if directories differ and file names match", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    const shelljsMvSpy = jest.spyOn(shelljs, "mv").mockImplementation();

    renameAndMoveFile({
      dryRun: false,
      srcFilePath: "/SrcDir/FileName.jpg",
      dstFilePath: "/DstDir/FileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(shelljsMvSpy).toHaveBeenCalledTimes(1);
    expect(shelljsMvSpy).toHaveBeenCalledWith(
      "-n",
      "/SrcDir/FileName.jpg",
      "/DstDir/FileName.jpg"
    );
  });

  it("should execute mv command if directories differ and file names differ", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    const shelljsMvSpy = jest.spyOn(shelljs, "mv").mockImplementation();

    renameAndMoveFile({
      dryRun: false,
      srcFilePath: "/SrcDir/SrcFileName.jpg",
      dstFilePath: "/DstDir/DstFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(shelljsMvSpy).toHaveBeenCalledTimes(1);
    expect(shelljsMvSpy).toHaveBeenCalledWith(
      "-n",
      "/SrcDir/SrcFileName.jpg",
      "/DstDir/DstFileName.jpg"
    );
  });

  it("should only log action during a dry run", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    const shelljsMvSpy = jest.spyOn(shelljs, "mv").mockImplementation();

    renameAndMoveFile({
      dryRun: true,
      srcFilePath: "/SrcDir/SrcFileName.jpg",
      dstFilePath: "/DstDir/DstFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(shelljsMvSpy).toHaveBeenCalledTimes(0);
  });
});
