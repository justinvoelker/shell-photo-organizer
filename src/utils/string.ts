export function stripExtraSlashes(value: string): string {
  return value
    .split("/")
    .filter((el) => el != "")
    .join("/");
}
