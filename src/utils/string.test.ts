import { stripExtraSlashes } from "./string";

describe("stripExtraSlashes", () => {
  it("should strip no slashes", () => {
    expect(stripExtraSlashes("Dir0/Dir1")).toBe("Dir0/Dir1");
  });

  it("should strip multiple slashes", () => {
    expect(stripExtraSlashes("///Dir0//Dir1///")).toBe("Dir0/Dir1");
  });
});
