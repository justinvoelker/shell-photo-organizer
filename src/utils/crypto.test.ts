import { getListHash } from "./crypto";

describe("getListHash", () => {
  it("should return hash of list", () => {
    const list = ["file1", "file2"];

    const hash = getListHash(list);

    expect(hash).toBe("ffcf740ac69f9cbdd3ae98d0de2174b7");
  });
});
