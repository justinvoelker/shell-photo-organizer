export function matchesPattern(
  value: string | unknown,
  regex: string
): boolean {
  return new RegExp(regex).test(value as string);
}
