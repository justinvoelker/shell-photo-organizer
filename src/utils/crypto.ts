import crypto from "crypto";

export function getListHash(list: string[]): string {
  return crypto.createHash("md5").update(JSON.stringify(list)).digest("hex");
}
