import { matchesPattern } from "./regex";

describe("matchesPattern", () => {
  it("should return true for matching pattern", () => {
    const match = matchesPattern("20210529T053346", "^(\\d{8}T\\d{6})$");

    expect(match).toBe(true);
  });

  it("should return false for matching pattern", () => {
    const match = matchesPattern("2021-05-29T05:33:46", "^(\\d{8}T\\d{6})$");

    expect(match).toBe(false);
  });
});
